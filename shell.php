<?php

/**
 * Hi !
 *
 * This is the Shell wrapping tool for this framework...
 * Dont modify or add code here, create a new file in the shell folder !
 *
 */

	// Main file for Shell call's
	// DO NOT add you own shell scripts in here!!

	error_reporting(E_ALL);
	ini_set('display_errors',1);
	define('CLI',true);
	define('debug',true);
	define('storefolder','app/storage');

	// Force time-zone
	date_default_timezone_set('Europe/Amsterdam');

	// Grep config file
	require_once 'app/config/config.php';

	$callType = substr(php_sapi_name(),0,3);

	// Check call type
	if($callType=='cli' or $config['ShellCompatible'] ==true)
	{
		if(count($argv)<2)
		{
			include 'app/config/config.php';

			print "\r\n";
			print "=========================================================="."\r\n";
			print " {$config['appname']} Commandline utility set "."\r\n";
			print "=========================================================="."\r\n";
			print " \r\n";
			print "Usage: php $argv[0] <modulename|list> <module-method | listmethods > [moduleparams] \r\n\r\n";

		}
		else
		{
			// Get some CLI Coloring
			require_once './app/helpers/clicolors.php';
			$colors = new Colors();

			if($argv[1]=="list")
			{
				print "List of available modules in shell: \r\n\r\n";
				$list = scandir('shell/');
				// Elem 0 and elem 1 are './' and '../'
				for($i = 2;$i<=count($list)-1;$i++)
				{
					print " - ".str_replace('.php','',$list[$i])."\r\n";
				}
				print "\r\n";
				return;
			}

			// Check if files needed are existing
			if(file_exists('./shell/'.$argv[1].".php"))
			{
				// Require in the requested modules
				require_once './shell/'.$argv[1].'.php';

				// Get overview of methods
				$methods = 	get_class_methods($argv[1]);

				// List all methods? or exec?
				if(@$argv[2]=="listmethods")
				{
					print "\r\nCommands available in module {$argv[1]}: \r\n\r\n";
					foreach($methods as $method)
					{
						print "- ".$method."\r\n";
					}
					print "\r\n";
				}
				else // Exec requested method
				{
					if(in_array(@$argv[2],$methods))
					{
						$instance = new $argv[1]();
						$return = call_user_func_array(array($instance,$argv[2]),array(@	$argv[3],@$argv[4]));

						if($return=="") { $return=" assuming false "; }
						print "\r\n- return value: [".$return."]\r\n";
					}
					else // requested method does not exists
					{
						print "\r\n";
						print "- ".$colors->getColoredString('Error','red')." Method ".@$argv[2]." does not exist in module $argv[1] ! \r\n";
						print "\r\n";
					}

				}
			}
			else
			{
				$error = $colors->getColoredString('Error','red');
				print "- ".$error." The module ".$argv[1]." can not be found\r\n";
			}
		}
	}
	else
	{
		print "Please, read the doc's! you should NOT call this file from a browser!";
	}
?>