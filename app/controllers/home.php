<?php
/**
 * This is the default caught route.
 * Please bare in mind that for now this can not be changed !
 *
 */

 /**
  * Home Controller
  *
  * extend the base-controller, register the index function,so we have a default page to catch every request that can not be routed.
  *
  */
class HomeController extends CoreController
{
	/**
	 * The index function, just renders a page and loads some stuff.
	 *
	 * @param string $name the title of the page
	 *
	 */
	public function index($name="")
	{
		$this->templater();
		print $this->templater->render('homeview.html',array(
			'appname'=>appname));
	}
}
