<?php
/**
 * Core Model
 *
 * This model is the core of every other model.
 * So it loads the database config and every other part that is needed globaly
 * 
 * ( It actualy inherets these items, but for the sake of argument etc )
 *
 */

 /**
  * CoreModel definition
  *
  */
class CoreModel
{
	/**
	 * Stack function, which makes it posisble to stack multiple models in case you 
	 * want to use multiple pieces of data at once for example.
	 * 
	 * @return object instance of requested model
	 */
	public function stack($model,$ref)
	{
		if(file_exists('../app/models/'.$model.'.php'))
		{
			require_once '../app/models/'.$model.'.php';

			// Generate string for naming convention
			$modelName = $model.'Model';

			$this->$ref = new $modelName($this->db);
		}
		else throw new Exception('Cant stack model '.$model.' File not found ', 1);
	}
}

?>