<?php
/**
 * Proforto Framework
 *
 * Application initalization for framework
 *
 */

/**
 * App initialization & Core routing
 *
 * What this class does is mostly check routing
 * If routing is not available it will route to home contorller and index method
 *
 * Also there are some core definitions in here, for example the default routing and the parsing of debug flags.
 *
 * If you have routing issue's though, there should no need to edit this file !!
 * most of the problems can be solved in the class itself.
 */
class App
{

	// Define default value's for core-routing
	/**
	 * @var string $controller the default controller to use
	 */
	protected $controller = "home";
	/**
	 * @var string $method the default method
	 */
	protected $method = "index";
	/**
	 * @var array $params the parameters to use for the method
	 */
	protected $params = array();


	/**
	 * Inits the MVC structure, parse the URL and map it to the controller
	 *
	 * @param boolean $api Set to API Mode, defaults to false.
	 */
	public function __construct($api=false)
	{
		// Set sesions
		session_start();

		// Load the configuration file
		if(file_exists('../app/config/config.php'))
		{
			include '../app/config/config.php';

			// Convert the debug flag to constante
			if($config['debug']) { define('debug',true); error_reporting(E_ALL); } else { define('debug',false); }
			// Set cli flag
			define('CLI',false);
			// Set app name
			define('appname',$config['appname']);
			// Define constant for basebath
			define('basepath',dirname(__FILE__));
			// Define constant for base-url
			//define('baseurl',$config['baseurl']);  ## Turns out that you need to supply the port in the path...
			define('baseurl',$config['baseurl'].':'.$_SERVER['SERVER_PORT']);
			// Public folder
			define('basepub',baseurl.'/public');
			// storage folder
			//define('storefolder',str_replace('/','/app/storage',getcwd()));
			define('storefolder',basepath."/../storage");
			// Define version number and build-data
			define('version',$config['version']);
			define('builddate',$config['builddate']);
			// Check if storage folder is ok
			if(!is_writable(storefolder)) { throw new Exception('storage folder ('.storefolder.' ) is not writeable!', 1);  }
		}
		else
		{
			throw new Exception("Config file not found!", 1);
		}

		// Parse URL
		$url = $this->parseUrl();

		// Check for API Specific controller if in API mode
		if($api)
		{
			if(file_exists('../app/api/'.$url[0].'.php'))
			{
				#print 'API Controller in API Folder '.$url[0];
				$this->controller = $url[0];
				unset($url[0]);
			}
			else
			{
				print json_encode(array('error'=>true,'error_msg'=>'No such contorller'));
				return false;
			}
		}
		else
		{
			if(file_exists('../app/controllers/'.$url[0].'.php'))
			{
				$this->controller = $url[0];
				unset($url[0]);
			}
		}

		// Before calling our own controller etc load the base controller.
		// ( No need to instanciate ! ( The extend will do that for us ))
		require_once '../app/core/Controller.php';

		// Now load the controller defined by browser
		if($api)
		{
			require_once '../app/api/'.$this->controller.'.php';
		} else require_once '../app/controllers/'.$this->controller.'.php';

		// Append Controller for naming convention
		$controllerName = $this->controller.'Controller';

		// Create new instance of our controller
		$this->controller = new $controllerName;

		// Maybe you would generate some code and have a __construct in your controller,
		// Fact of the matter is though, that it would break the framework.
		$reflector = new ReflectionMethod($this->controller,'__construct');
		if($reflector->getDeclaringClass()->getName()!='CoreController')
		{
			throw new Exception('Cant overload construct method in '.$reflector->getDeclaringClass()->getName(), 1);

		}

		// Check if API mode.
		if($api)
		{
			// First off, set the header to JSON
			header( 'Content-Type: text/json' );
			if(method_exists($this->controller,@$url[1]))
			{
				$this->method = $url[1];
				unset($url[1]);
			}
			else
			{
				if(method_exists($this->controller,'index'))
				{
					$this->method = 'index';
				}
				else
				{
					print json_encode(array('error'=>true,'error_msg'=>'No such method in controller '.$controllerName));
					return false;
				}
			}
		}

		// If a method is defined
		if(isset($url[1]))
		{
			if(count($_POST)!=0)
			{
				if(method_exists($this->controller,"post_".$url[1])) // check for post-specific routes
				{
					$this->method = "post_".$url[1];
					unset($url[1]);
				}
			}
			elseif(method_exists($this->controller,'get_'.$url[1])) // check for get specific posts.
			{
				$this->method = "get_".$url[1];
				unset($url[1]);
			}
			else // In case there is no prefix
			{
				if(method_exists($this->controller,$url[1]))
				{
					$this->method = $url[1];
					unset($url[1]);
				}
			}
		}

		$this->params = $url ? array_values($url) : array(); // if there are no entry's in the array, this will give an empty array to prevent error's

		// Call the MVC URI
		call_user_func_array(array($this->controller,$this->method),$this->params);
	}

	/**
	 * parseUrl
	 *
	 * Parse the URL to an array which we can throw in the core-router
	 */
	protected function parseUrl()
	{
		if(isset($_GET['url']))
		{
	
			// Yank of the trailing slash, filter it (sanitize) , replace spaces by underscores ,and explode it on the slash :-)			
			$filter =  explode('/',filter_var(rtrim(str_replace(' ','_',$_GET['url']),'/'),FILTER_SANITIZE_URL));

			# debug 
			#var_dump($filter);

			return $filter;
		}
	}
}
