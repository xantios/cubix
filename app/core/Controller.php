<?php

/**
 * The core controller
 *
 * This is where most of the loading is happening
 *
 */

 /**
  * The core controller class
  */
class CoreController
{
	/**
	 * Templater
	 * @var $templater refrence to the template engine
	 */
	protected $templater;
	/**
	 * Config class
	 * @var $config the ref to the configger class
	 */
	protected $config;
	/**
	 * Database class
	 * @var $db the DB model/class.
	 */
	protected $db;
	/**
	 * PHPMailer instance
	 * @var $mail the PHPMailer instance
	 */

	/**
	 * Base inits for the base controller
	 */
	 public function __construct()
	 {
	 	//print 'Core Controller loaded';

		// Require in the base twig loader.
		require_once '../app/vendor/twig/lib/Twig/Autoloader.php';
		Twig_Autoloader::register();

		// Register the exception handler
		set_exception_handler(array($this,'exceptionhandler'));

		// Load the configuration file to config object
		if(file_exists('../app/config/config.php'))
		{
			include '../app/config/config.php';
			$this->config = $config;
		}
		else
		{
			throw new Exception("Config file not found!", 1);
		}

		// Load base model
		require_once '../app/core/Model.php';

		// Create instance of the DB, dont connect it though.
		require_once '../app/core/Database.php';
		$this->db = new db();

		// Register the autoloader
		spl_autoload_register(array($this,'autoloader'));

		// Check if there is any data in the auto-load model array
		if(is_array($loadmodels)) {
			foreach($loadmodels as $model)
			{
				$this->model($model);
			}
		}

		// And for the helpers
		if(is_array($loadhelpers))
		{
			foreach ($loadhelpers as $helper)
			{
				$this->helper($helper);
			}
		}
	 }

	 /**
	  * Just in case the autoloader is messing with you'r mind, you CAN disable it :-)
	  *
	 */
	 public function core_unload_autoloader()
	 {
	 	spl_autoload_unregister('autoloader');
	 	return true;
	 }

	 /**
	 * and if you changed your mind about the autoloader, lets re-reg
	 */
	 public function core_reload_autoloader()
	 {
	 	$this->core_unload_autoloader();
	 	spl_autoload_register(array($this,'autoloader'));
	 }

	 /**
	  * Autoloader
	  *
	  * Figures the most common ways to catch the includes needed for a object instance
	  *
	  * @param string $class the class to load
	  */
	  public function autoloader($class="")
	  {
	  	// To make it possible to overrule base info, first check if external ( user defined ) stuff is available

	  	// debug
	  	//print $class." not loaded, please reuire files before you init them you dummy\r\n";

		// Check if we can track down the file in the models
		if(file_exists('../app/models/'.$class.'.php'))
		{
			require_once '../app/models/'.$class.'.php';
			return;
			//throw new Exception("Requested class ".$class." is a model, please use the model-loader in base", 1);
		}

		// Check if helper
		if(file_exists('../app/helpers/'.$class.'.php'))
		{
			require_once '../app/helpers/'.$class.'.php';
			return;
			//throw new Exception('Requested class '.$class.' is a helper, please use the helper-loader in base',1);
		}

		// Is it one of our own guys ?
		if(file_exists('../app/core/libs/'.$class.'.php'))
		{
			require '../app/core/libs/'.$class.'.php';
			return;
		}

		// and maybe some helping for 'weird' autoloaders from vendor-code's
		if(file_exists('../app/vendor/'.strtolower($class).'/include/autoload.inc.php'))
		{
			require '../app/vendor/'.strtolower($class).'/include/autoload.inc.php';
			return;
		}

		if(file_exists('../app/vendor/'.strtolower($class).'/autoload.php'))
		{
			require '../app/vendor/'.strtolower($class).'/autoload.php';
			return;
		}

		throw new Exception('Requested class '.$class.' can not be found, please use the build in loaders, and check if the file exists');

	  }

	/**
	 * load a model
	 *
	 * @param string $model the model to load
	 * @param  string  $loadAs instead of using $this->$modelname it can use any name you desire.
	 * @return object
	 */
	public function model($model,$loadAs="")
	{
		if(file_exists('../app/models/'.$model.'.php'))
		{
			require_once '../app/models/'.$model.'.php';

			// Generate string for naming convention
			$modelName = $model.'Model';

			if($loadAs!="")
			{
				$this->$loadAs = new $modelName($this->db);
			}
			else
			{
			 	$this->$model = new $modelName($this->db);
			}
		}
		else throw new Exception('Cant load model '.$model.' File not found ', 1);

	}

	/**
	 * Load a helper
	 *
	 * @param string $helper the helper to load
	 * @return bool
	 */
	 public function helper($helper)
	 {
	 	if(file_exists('../app/helpers/'.$helper.'.php'))
		{
			require_once '../app/helpers/'.$helper.'.php';
			$this->$helper = new $helper;
			return true;
		}
		return false;
	 }

	 /**
	  * Default exception handler
	  *
	  * @param exception $e the exception to handle
	  * @return void
	  *
	  */
	  public function exceptionhandler(exception $e)
	  {
	  	if(debug)
		{
			// Cause this should handle an exception I want to do as much as possible in one function.
			// This makes it a tedious long story, but it sure is more stable

		  	// /check if a 'template' alike file is available
		  	if(!file_exists('../app/core/ExceptionPage.html'))
			{
				print "<h1>Congratz! U made it break!</h1>";
		  		print $e->getMessage()."<br /><br />";
				print $e->getTraceAsString();
			}
			else
			{
				// Yank data from disk
				$data=file_get_contents('../app/core/ExceptionPage.html');

				// Define a buffer to save some data from the 'crashing' file
				$fileSnippetArray=array();

				// Yank some data from the file wich is triggering this exception
				$file = fopen($e->getFile(), "r");
				// Read all data to a line, push it to single element in array
				while(!feof($file)) { $fileSnippetArray[] = fgets($file); }
				// Drop the file pointer
				fclose($file);
				// String to push output
				$fileSnippet='';
				// Convert array to string-snippet
				// @TODO: Calculate offset here.
				for($i = $e->getLine()-8;$i<=$e->getLine()+8;$i++)  {  $fileSnippet .= @$fileSnippetArray[$i]; }

				// Make a bit of html
				$exceptiondata = "<h2>Message</h2>";
				$exceptiondata.="<p>".$e->getMessage()."</p>";
				$exceptiondata .="<p>".$e->getFile().":".$e->getLine()."</p>";

				$exceptiondata .= "<h2>Trace</h2>";
				$exceptiondata .="<p><pre>".$e->getTraceAsString()."</pre></p>";

				$exceptiondata .= "<h2>Code</h2>";
				$exceptiondata .= '<p class="snippet">'.highlight_string('<?php '.$fileSnippet."?>",true)."</p>";

				print str_replace('{{exception}}',$exceptiondata,$data);
			}
		}
		else // If debug is set to false
		{
			print "An exception occured, please contact your administrator";
		}
	}

	/**
	 * Gets all methods available in the class specified
	 * @param  object  $class         the object ref of the class
	 * @param  boolean $filterInherit filter inherentice methods?
	 * @return array                 array of methods in scope
	 */
	public function getMethods($class,$filterInherit=true)
	{
		if($filterInherit==true)
		{
			// Thanks to onesimus
			// http://php.net/manual/en/function.get-class-methods.php#43379
    			$array1 = get_class_methods($class);
    			if($parent_class = get_parent_class($class))
    			{
        				$array2 = get_class_methods($parent_class);
        				$array3 = array_diff($array1, $array2);
    			}
    			else
    			{
        				$array3 = $array1;
    			}
    			return($array3);
    		}
    		else { return get_class_methods($class); }
	}

	/**
	 * Load the templater
	 *
	 * Create a new instance of the tempalter if needed,
	 * else returns the object refrence to the current instance.
	 *
	 * @return object
	 */
	public function templater()
	{
		// If the templater is already loaded
		if(isset($this->templater)) return $this->templater;
		// Else load it right now
		else
		{
			// Load twig up
			$loader = new Twig_Loader_Filesystem('../app/views/');
			$twig = new Twig_Environment($loader);
			// Make it available in the class
			$this->templater = $twig;
			return $this->templater;
		}
	}
}
