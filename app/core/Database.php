<?php

/**
 * The main database functionality gets implemented here.
 *
 * this is mostly inspired by RedBeanPHP.
 * see: http://redbeanphp.com/crud for more info!
 *
 */

/**
 * this class is initiated by the core model,
 * and with a dependency injector its pushed into the models.
 */
class db
{
	// Connection reference

	/**
	 * @var object $connection the connection reference is stored here
	 */
	private $connection = null;
	/**
	 * @var struct $tableStruct the structure of the current table is stored here.
	 */
	private $tableStruct = array();
	/**
	 * @var string $table the current table
	 */
	 private $table;

	/**
	 * __construct
	 *
	 * Just call the connect function
	 */
	 public function __construct() { $this->connect(); }

	 /**
	  * connect
	  *
	  * searchs for the configuration file, and checks if the credentials are valid
	  *
	  * @param none
	  * @return object
	  * @throws exception
	  */
	 private function connect()
	 {
	 	// Check if running in CLI, if so the relative path to the config file's differ!
	 	if(CLI==true) { $path = 'app/config/database.php'; } else { $path = '../app/config/database.php'; }

	 	// Yank the config data from disk
	 	if(!file_exists($path))
		{
			throw new Exception('Database config file not found');
		}
		else
		{
			// Require the config params
			require_once $path;
			if($database['enabled']!=false)
			{
				// Connect
				$this->connection = new mysqli($database['server'],$database['username'],$database['password'],$database['database'],$database['port']);
				// Remove configuration propperties from memory
				unset($database);
				// Check connection state
				if($this->connection->connect_errno)
				{
					if(!debug) {
						throw new Exception('Cant connect to the database.');
					} else	{
						throw new Exception("Cant connect to database Err-num: ".$this->connection->connect_errno." Error: ".$this->connection->connect_error);
					}
				}
			}
		}
	 }

	 /**
	  * setDispensed
	  *
	  * Internal function to set the current dispensed table
	  *
	  * @param string $table table-name
	  * @return bool
	  */
	 private function setDispensed($table)
	 {
	 	return $this->dispensed = $table;
	 }

	 /**
	  * getDispensed
	  *
	  * Gets the currently dispended table name ( as string )
	  *
	  * @return string
	 */
	 public function getDispensed()
	 {
	 	return $this->dispensed;
	 }

	 /**
	  * dispense
	  *
	  * Dispense the table ( make it available for CRUD operations )
	  *
	  * @param string $tableName the name of the table to dispense
	  * @return object
	  */
	 public function dispense($tableName)
	 {
	 	// Set the dispensed table
	 	$this->setDispensed($tableName);

	 	// the query
	 	$query = 'SHOW COLUMNS FROM '.$tableName;

		// exec query
		$result = $this->connection->query($query);

		// Unset the current table structure if available
		unset($this->tableStruct);

		// Yep.. countah!
		$x=0;

		// Get data
		while($tmp = $result->fetch_array())
		{
			// Sanitize data
			// - Replace Null (yes/no with true/false)
			$tmp['Null'] = str_replace("YES",TRUE,$tmp['Null']);
			$tmp['Null'] = str_replace("NO",FALSE,$tmp['Null']);
			// - Replace the Primary string PRI with bool
			$tmp['Key'] = str_replace('PRI',TRUE,$tmp['Key']);
			if($tmp['Key'] != TRUE) { $tmp['Key'] = false; }
			// - check the extra field for auto_increment
			if($tmp['Extra'] == "auto_increment") { $tmp['ai'] = true;} else { $tmp['ai'] = false; }

			// Explode
			$type = explode(" ",$tmp['Type']);
			// Yank out the numbers
			$type = str_replace(preg_replace("/[^0-9]/","",$tmp['Type']),"",$type[0]);
			// Yank of the ()
			$type = str_replace('()',"",$type);

			// Check if singed.
			$tmp['Type'] = explode(' ',$tmp['Type']);

			//print " SING = ".$tmp['Type'][1];

			if(@$tmp['Type'][1]!="unsigned")
			{
				$tmp['Singed'] = false;
			} else $tmp['Singed'] = true;

			// Map structure to $arrayName = array('' => , );
			$this->tableStruct[$x]['Name']			= $tmp['Field'];
			$this->tableStruct[$x]['Type']	 		= $type;
			$this->tableStruct[$x]['Null'] 			= $tmp['Null'];
			$this->tableStruct[$x]['Primary'] 		= $tmp['Key'];
			$this->tableStruct[$x]['Default'] 		= $tmp['Default'];
			$this->tableStruct[$x]['AutoIncrement'] = $tmp['ai'];
			$this->tableStruct[$x]['MaxLength']		= $tmp['MaxLength'] = preg_replace("/[^0-9]/","",$tmp['Type'])[0];  // This is valid syntax, but Aptana keeps winig about it...
			$this->tableStruct[$x]['Singed']		= $tmp['Singed'];

			$x++;
		}

		// Store the current table to table var
		$this->table = $tableName;

	 	return (object)array();
	 }

	 /**
	  * Store object to database
	  *
	  * @param store $object the object to store to the dispensed table
	  */
	 public function store($object)
	 {
 		// Convert object to array
 		$object = (array)$object;
		$object = $this->ParseStruct($object);
		// if an ID is supplied, assume there needs to be an update
		// if not, generate INSERT query.
		if(@$object['id']!=0)
		{
			return $this->update($object);
		}
		else
		{
			return $this->create($object);
		}
	}

	 /**
	  * Create fucntion
	  *
	  * the C in CRUD ;-)
	  *
	  * @param $object An object to insert
	  */
	  private function create($object)
	 {
	 	// Sanitize the object
		$object = $this->sanitize($object);

		// Parse the struct so the object is type-corrected
		$this->ParseStruct($object, $this->tableStruct);

	 	// Generate the query
	 	// ( Wrap everything in backtics for weird collum-names like 'when' or 'for' )
		$query = "INSERT INTO ".$this->table." (`";
		$query .= implode('`,`',array_keys($object)).'`';

		// Add values to query
		$query.=") VALUES (";
		$query .=implode(',',$object);

		// Add closing tag
		$query .= ')';

		if($this->connection->query($query))
		{
			//return $this->connection->insert_id;
			return true;
		} else return null;
	 }

	 /**
	  * Update information in the database.
	  *
	  * @param array $newdata the new data for the update in DB
	  *
	  */
	 private function update($newdata)
	 {
		// First get the current data for this ID, so we can compare.
		// if data fields are missing from the given object, we can merge them where needed.
		$query = 'SELECT * FROM '.$this->table.' WHERE id = \''.$newdata['id'].'\'';
		$currentData = $this->connection->query($query);
		$currentData = $currentData->fetch_assoc();
		// Merge data
		$data = array_merge($currentData,$newdata);
		// Build a query
		$query = "UPDATE ".$this->table." SET ";
		foreach($data as $Key => $Value)
		{
			$query.=$Key."= '".$Value."',";
		}

		$query = substr($query,0,strlen($query)-1)." WHERE id = ".$currentData['id'];

		//print "QUERY = ".$query;

		$this->connection->query($query);
		if($this->connection->error)
		{
			print $this->connection->error;
			return 0;
		}
		return $this->connection->insert_id;
	 }
	 /**
	  * Sanitizer for MySQL
	  *
	  * returns the original object, but sanitized.
	  *
	  * @param object $object The object to sanitize.
	  * @return object
	  *
	  */
	  private function sanitize($object)
	  {
		// Check if counts match
		// NOTICE: This is the most common error !!!
		if(count($object)!=count($this->tableStruct))
		{
			if(debug)
			{
				print "\r\n\r\n- Source object: ";
				var_dump($object);
				print "\r\n\r\n- Structure to match: ";
				var_dump($this->tableStruct);
				print "\r\n";

				throw new Exception('Invalid database operation');
			} else {
				throw new Exception('Invalid database operation');
			}
		}

	  	// Clean up the array and do some sanytizing
		array_walk($object,function(&$object,$key)
		{
			if($object == "") { $object = "''"; }
			if(is_string($object)) { $object = "'".$object."'"; }
		});
		return $object;
	  }

	 /**
	  * Retrieve function
	  *
	  * Read some data from DB.
	  *
	  * @param string $table the table name
	  * @param int $id the id of the field
	  *
	  * @return object
	  */
	  public function load($table,$id)
	  {
	  	$query = "SELECT * FROM ".$table." WHERE id = '".$id."'";

		$result = $this->connection->query($query);
		if($result)
		{
			return (object)$result->fetch_assoc();
		} else return (object)array();
	  }

	  /**
	   * Delete function
	   *
	   * Nothing exciting I guess
	   *
	   * @param int $id ID of item to trash
	   */
	   public function Trash($id)
	   {
	   		$query = "DELETE FROM ".$this->table." WHERE id = ".$id;
			$this->connection->query($query);
	   }

	   /**
	    * Finder
	    *
	    * find a single item in Database
	    *
	    * @param string $table The table to search in
	    * @param string $select The seleciton query.
	    * @param  string $sort The prefered sort method ( ASC or DESC )
	    *
	    * @return object
	    */
	    public function find($table,$select,$sort='DESC')
		{
			if(strstr($select,";")==false)
			{
				$query = 'SELECT * FROM '.$this->connection->real_escape_string($table).' WHERE '.$select." LIMIT 1";
				if($result = $this->connection->query($query))
				{
					return $result->fetch_object();
				}
				else
				{
					if(debug) {
					print $this->connection->error;
					}

					return (object)array();
				}
			} else throw new Exception('Potential database hacking', 1);
		}

		/**
		 * Finder
		 *
		 * find all items that match the selector
		 *
		 * @param string $table The table to look in to
		 * @param string $select The selector to use.
		 * @param string  $sort the sort method ( ASC or DESC )
		 *
		 * @return array
		 */
		public function findAll($table,$select="",$sort='')
		{
			if($select!="")
			{
				$query = 'SELECT * FROM '.$this->connection->real_escape_string($table).' WHERE '.$select;
			}
			else
			{
				$query = 'SELECT * FROM '.$this->connection->real_escape_string($table);
			}

			if($sort!="")
			{
				$query .= ' ORDER BY id '.$sort;
			}

			// Start with an empty array, in case the query doesnt give any data back
			// PHP will not give a E_NOTICE
			$results=array();

			if($result = $this->connection->query($query))
			{
				while($data = $result->fetch_object())
				{
					$results[] = $data;
				}
				return $results;
			}
			else
			{
				if(debug) {
				print $this->connection->error;
				}

				return array();
			}
		}
	  /**
	   * Query function
	   *
	   * ONLY FOR POWER-USERS !!!!!
	   *
	   * @param  string $q the query to exec.
	   *
	  */
	  public function query($q)
	  {
	  	$result = $this->connection->query($q);

	  	while($data = $result->fetch_object())
		{
			$results[] = $data;
		}
		return @$results;
	  }

	  /**
	   * Structure Parser
	   *
	   * Push in an object and an structure
	   *
	   * @param object $object An object to parse, structure is caught from scope
	   */
	   public function ParseStruct($object)
	   {

			// Loop through the user-data
			array_walk($object,function(&$object,$key)
			{
				// $key 	=	label ( id. username, etc )
				// $object	=	value ( 0, xantios, etc )

				// Loop through the table structure and check if the label ($key) matches the current key
				foreach($this->tableStruct as $struct)
				{
					//print " Value = ".$value['Name']." | ".$key."</br >";
					if($struct['Name']==$key && $key!==0)
					{
						// Fix length ( if available)
						if($struct['MaxLength']!="")
						{
							if(strlen($object)>$struct['MaxLength'])
							{
								//print "MAXLENGHT EQ ".$struct['MaxLength'];
								$object = substr($object,0,$struct['MaxLength']);
							}
						}

						// Is Null?
						if($object=='') { $object = null; }

						// varchar is not a PHP type, string is ...
						$struct['Type'] = str_replace('varchar', 'string', $struct['Type']);
						// datetime is not a type... convert to string
						$struct['Type'] = str_replace('datetime','string',$struct['Type']);
						// char is just a small string :-P
						$struct['Type'] = str_replace('char', 'string',$struct['Type']);
						// text is just a long stirng >.>
						$struct['Type'] = str_replace('text','string',$struct['Type']);
						// tiny-int is just an int
						$struct['Type'] = str_replace('tinyint','int',$struct['Type']);

						// debug output
						//print "The Key: ".$key." (".$object.") is casted to ".$struct['Type']."<br />";
						//print "Type = ".$struct['Type'];
						settype($object,$struct['Type']);
					}
				}
			});
			return $object;
	   }
}
?>