<?php
/**
 * Just a little JSON Helper
 */

 /**
  * Helpers dont have a core-helper ;-)
  */
class json
{
	/**
	 * Generate a unix timestamp
	 */
	function timestamp() { return time(); }
	/**
	 * Generates a human-readable time
	 */
	function time() { return date('G:i:s'); }
	/**
	 * Generate a human-readable time
	 */
	function date() { return date('Ymd'); }

	/**
	 * token
	 *
	 * returns a randomized token of static length
	 *
	 * @param int $length the length of the key, defaults to 42
	 * @return string
	 */
	function token($length=42)
	{
	        //fallback to mt_rand if php < 5.3 or no openssl available
	        $characters = '0123456789';
	        $characters .= 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
	        $charactersLength = strlen($characters)-1;
	        $token = '';

	        //select some random characters
	        for ($i = 0; $i < $length; $i++) {
	            $token .= $characters[mt_rand(0, $charactersLength)];
	        }

	        return $token;
	}
}
?>