<?php
/**
 * CLI-Colors file
 *
 * Thanks for this fly out to: http://www.if-not-true-then-false.com/2010/php-class-for-coloring-php-command-line-cli-scripts-output-php-output-colorizing-using-bash-shell-colors/
 *
 */

 	/**
	 * Cli-color class
	 */
	class Colors
	{
		/**
		 * @var array $foreground_colors Storage for foreground colors
		 * @var array $background_colors Storage for backgorund colors
		 */
		private $foreground_colors = array(),
				$background_colors = array();

		/**
		 * cli-error
		 *
		 * Quick function to print an error to cli
		 *
		 * @param string $error Error message
		 */
 		public function clierror($error)
		{
			print $this->getColoredString(":: Error   : (".strlen($error).") : ".$error."\r\n",'red');
		}

		/**
		 * Same but with warnings...
		 *
		 * @param string $warn Warning message
		 */
		public function cliwarning($warn)
		{
			print $this->getColoredString(":: Warning : (".strlen($warn).") : ".$warn."\r\n",'yellow');
		}

		/**
		 * Same but with info...
		 *
		 * @param string $info Info message
		 */
		public function cliinfo($info)
		{
			print $this->getColoredString(":: Info   : (".strlen($info).") : ".$info." \r\n",'light_blue');
		}

		/**
		 * And a noticer
		 *
		 * @param string $msg Notice message
		*/
		public function clinotice($msg)
		{
			print $this->getColoredString(':: Notice : ('.strlen($msg).") : ".$msg."\r\n",'cyan');
		}

		/**
		 * Debug messenger
		 *
		 * @param string $type Message identifier
		 * @param string $msg Message
		 */
		public function climsg($type,$msg)
		{
			print $this->getColoredString(':: '.$type." : (".strlen($msg).") : ".$msg."\r\n",'light_green');
		}

		/**
		 * Define all color's
		 * @return void
		 */
		public function __construct() {
			// Set up shell colors
			$this->foreground_colors['black'] = '0;30';
			$this->foreground_colors['dark_gray'] = '1;30';
			$this->foreground_colors['blue'] = '0;34';
			$this->foreground_colors['light_blue'] = '1;34';
			$this->foreground_colors['green'] = '0;32';
			$this->foreground_colors['light_green'] = '1;32';
			$this->foreground_colors['cyan'] = '0;36';
			$this->foreground_colors['light_cyan'] = '1;36';
			$this->foreground_colors['red'] = '0;31';
			$this->foreground_colors['light_red'] = '1;31';
			$this->foreground_colors['purple'] = '0;35';
			$this->foreground_colors['light_purple'] = '1;35';
			$this->foreground_colors['brown'] = '0;33';
			$this->foreground_colors['yellow'] = '1;33';
			$this->foreground_colors['light_gray'] = '0;37';
			$this->foreground_colors['white'] = '1;37';

			$this->background_colors['black'] = '40';
			$this->background_colors['red'] = '41';
			$this->background_colors['green'] = '42';
			$this->background_colors['yellow'] = '43';
			$this->background_colors['blue'] = '44';
			$this->background_colors['magenta'] = '45';
			$this->background_colors['cyan'] = '46';
			$this->background_colors['light_gray'] = '47';
		}

		/**
		 * getColoredString
		 *
		 * Get's the string in color
		 *
		 * @param string $string string to colorize
		 * @param string $foreground_color guess....
		 * @param string $background_color gues again...
		 *
		 * @return string
		 */
		public function getColoredString($string, $foreground_color = null, $background_color = null) {
			$colored_string = "";

			// Check if given foreground color found
			if (isset($this->foreground_colors[$foreground_color])) {
				$colored_string .= "\033[" . $this->foreground_colors[$foreground_color] . "m";
			}
			// Check if given background color found
			if (isset($this->background_colors[$background_color])) {
				$colored_string .= "\033[" . $this->background_colors[$background_color] . "m";
			}

			// Add string and end coloring
			$colored_string .=  $string . "\033[0m";

			return $colored_string;
		}

		/**
		 * getForegroundColors
		 *
		 * Gets ya an array of colors to use
		 *
		 * @return array
		 */
		public function getForegroundColors() {
			return array_keys($this->foreground_colors);
		}

		/**
		 * getBackgroundColors
		 *
		 * Gets ya an array of colors to use
		 *
		 * @return array
		 */
		public function getBackgroundColors() {
			return array_keys($this->background_colors);
		}
}
?>