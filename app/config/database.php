<?php
/**
 * Set some parameters to connect to the database.
 * Hint: Default MySQL port is 3306
 */
$database['enabled'] = false; # First set this to true if you want to use the database.
$database['server'] = "127.0.0.1"; # If you're using localhost, please use 127.0.0.1 instead ( There seems a bit of a problem here: http://stackoverflow.com/questions/13870362/php-mysql-test-database-server )
$database['username'] = "username";
$database['password'] = "password";
$database['database'] = "mydatabase";
$database['port'] = 3306;

?>
