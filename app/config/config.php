<?php
/**
 * Global configuration file for the framework.
 * nothing really exciting going on, value's should be pretty self explanitory
 */
	// Global configuration for your app

	// Set debug mode ( true or false)
	$config['debug'] = true;

	// Set application mode ( devel or production )
	$config['mode'] = "devel";

	// Define a name for your application
	$config['appname'] = 'CubixCore';

	// Version and build date
	$config['version'] = '1.0.2'; # define as a string, not as an int/float.
	$config['builddate']='09-10-2014 15:40:12';

	// Base URL , no port-number nor trailing slash please ! :-)
	$config['baseurl'] = "http://changeMeInConfigDotPHP.tld";

	// Set shel compatibility ( for example: cPanel does some weird callings in cron-jobs )
	$config['ShellCompatible']=true;

	// To load some of your component for every page, add them in the correct array here.
	// Please notice though, that performance wise only load elements
	// if you really need them a lot. it does take some work to load (big) models for example.

	// Default models
	$loadmodels = array();

	// Default helpers
	$loadhelpers = array();

?>