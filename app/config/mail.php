<?php
/**
 * Mail configuration
 * 
 * Just sets the basics for connecting to a Mail-server
 * 
 */
// Mail server config
$mailserver = 'smtp.gmail.com';
$mailport = 587; // defaults to 25
$mailsecurity = 'tls'; // can be tls,ssl or none ( defaults to none )
$mailauth = true; // can be true or false ( defaults to false )

// Mail user config (if mail auth is needded )
$mailuser = 'MyMail@ChangeMeInMailDotPhp.tld';
$mailpass = 'ExamplePassword';

// Mail From
$mailfrom 			= "MyApp";	// Defaults to appname
$mailfromaddress 	= "no-reply@ChangeMeInMailDotPhp.tld"; 


?>