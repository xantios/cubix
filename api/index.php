<?php
/**
 * Bootstrapper for the API layer of the framework.
 *
 * Relevant controllers should go in app/api/controllername.php 
 */

// Bootstrap our framework
require_once "../app/init.php";
$app = new app(true);
?>